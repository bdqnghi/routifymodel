package z.routify.routifymodel.shared.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 12:05 PM
 */
@Entity("users")
public class User {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    private ObjectId id;
    private Date createDate;
    private Date modifiedDate;
    private List<ObjectId> roleIds = new ArrayList<ObjectId>();
    private String password;
    private String screenName;
    @Indexed(unique = true) private String emailAddress;
    private String facebookId;
    private String openId;
    private String firstName;
    private String lastName;
    private Date loginDate;
    private Date lastLoginDate;
    private Integer failedLoginAttempts;
    private Boolean emailAddressVerified;
    private Integer status;
    private String tokenPart;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Integer getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(Integer failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    public Boolean getEmailAddressVerified() {
        return emailAddressVerified;
    }

    public void setEmailAddressVerified(Boolean emailAddressVerified) {
        this.emailAddressVerified = emailAddressVerified;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ObjectId> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<ObjectId> roleIds) {
        this.roleIds = roleIds;
    }

    public String getTokenPart() {
        return tokenPart;
    }

    public void setTokenPart(String tokenPart) {
        this.tokenPart = tokenPart;
    }
}
