package z.routify.routifymodel.shared.models;

import java.util.Date;

/**
 * User: Zun
 * Date: 6/11/14
 * Time: 6:30 PM
 */
public class PlaceReview {
    private Object aspects;
    private String author_name;
    private String author_url;
    private String language;
    private int rating;
    private String text;
    private Date time;
    private String shortComment;
    private String goodPoints;

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_url() {
        return author_url;
    }

    public void setAuthor_url(String author_url) {
        this.author_url = author_url;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Object getAspects() {
        return aspects;
    }

    public void setAspects(Object aspects) {
        this.aspects = aspects;
    }

    public void setShortComment(String shortComment) {
        this.shortComment = shortComment;
    }

    public String getShortComment() {
        return shortComment;
    }

    public void setGoodPoints(String goodPoints) {
        this.goodPoints = goodPoints;
    }

    public String getGoodPoints() {
        return goodPoints;
    }
}
