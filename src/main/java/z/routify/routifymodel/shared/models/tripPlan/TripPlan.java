package z.routify.routifymodel.shared.models.tripPlan;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.List;

/**
 * User: zWork
 * Date: 15.8.2014
 * Time: 10:03
 */
@Entity("tripplans")
public class TripPlan {
    @Id private ObjectId id;
    private String name;
    private List<TripNode> tripNodes;
    private List<TripLeg> tripLegs;

    public ObjectId getId() {
        return this.id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TripNode> getTripNodes() {
        return tripNodes;
    }

    public void setTripNodes(List<TripNode> tripNodes) {
        this.tripNodes = tripNodes;
    }

    public List<TripLeg> getTripLegs() {
        return tripLegs;
    }

    public void setTripLegs(List<TripLeg> tripLegs) {
        this.tripLegs = tripLegs;
    }
}
