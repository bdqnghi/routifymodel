package z.routify.routifymodel.shared.models;

/**
 * User: Zun
 * Date: 6/11/14
 * Time: 6:49 PM
 */
public class PlaceTime {
    private int day;
    private String time;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
