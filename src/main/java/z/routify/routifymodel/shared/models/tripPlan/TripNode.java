package z.routify.routifymodel.shared.models.tripPlan;

import org.mongodb.morphia.annotations.Id;
import z.routify.routifymodel.shared.models.Location;
import z.routify.routifymodel.shared.models.PlaceDetail;

import java.util.List;

/**
 * Created by CHRIS on 9/08/2014.
 */
public class TripNode {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    @org.jongo.marshall.jackson.oid.ObjectId
    private String id;
    private int index;
    private String label;
    private String address;
    private Location geoData;
    List<NoteItem> notes;
    TimePoint arrival;
    TimePoint departure;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Location getGeoData() {
        return geoData;
    }

    public void setGeoData(Location geoData) {
        this.geoData = geoData;
    }

    public List<NoteItem> getNotes() {
        return notes;
    }

    public void setNotes(List<NoteItem> notes) {
        this.notes = notes;
    }

    public TimePoint getArrival() {
        return arrival;
    }

    public void setArrival(TimePoint arrival) {
        this.arrival = arrival;
    }

    public TimePoint getDeparture() {
        return departure;
    }

    public void setDeparture(TimePoint departure) {
        this.departure = departure;
    }
}
