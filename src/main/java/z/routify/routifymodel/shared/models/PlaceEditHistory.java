package z.routify.routifymodel.shared.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.*;

/**
 * User: Zun
 * Date: 6/7/14
 * Time: 12:05 PM
 */
@Entity("PlaceEditHistory")
public class PlaceEditHistory {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    private ObjectId id;
    private ObjectId placeId;
    private String createdUserEmail;
    private Date createDate;

    private List<PlaceEditHistoryItem> historyItems = new ArrayList<PlaceEditHistoryItem>();

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getPlaceId() {
        return placeId;
    }

    public void setPlaceId(ObjectId placeId) {
        this.placeId = placeId;
    }

    public String getCreatedUserEmail() {
        return createdUserEmail;
    }

    public void setCreatedUserEmail(String createdUserEmail) {
        this.createdUserEmail = createdUserEmail;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<PlaceEditHistoryItem> getHistoryItems() {
        return historyItems;
    }

    public void setHistoryItems(List<PlaceEditHistoryItem> historyItems) {
        this.historyItems = historyItems;
    }
}