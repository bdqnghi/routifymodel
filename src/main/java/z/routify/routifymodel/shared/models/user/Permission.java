package z.routify.routifymodel.shared.models.user;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * User: Zun
 * Date: 9/12/2014
 * Time: 11:30 PM
 */
@Entity("permission")
public class Permission {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    private org.bson.types.ObjectId id;
    private String name;
    private String description;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
