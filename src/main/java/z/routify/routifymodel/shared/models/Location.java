package z.routify.routifymodel.shared.models;

/**
 * User: Zun
 * Date: 6/8/14
 * Time: 4:11 PM
 */
public class Location {
    private double lng;
    private double lat;

    public Location() {
    }

    public Location(double lng, double lat) {
        this.lng = lng;
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
