package z.routify.routifymodel.shared.models.tripPlan;

import z.routify.routifymodel.shared.models.Location;

/**
 * Created by CHRIS on 12/08/2014.
 */
public class LegStep {
    private String html_instructions;
    private CompositeValue distance;
    private CompositeValue duration;
    private Location start_location;
    private Location end_location;
}
