package z.routify.routifymodel.shared.models;

import java.util.Date;

/**
 * User: Zun
 * Date: 9/17/2014
 * Time: 12:50 AM
 */
public class PlaceEditHistoryItem {
    private String email;
    private Date editTime;

    public PlaceEditHistoryItem() {
    }

    public PlaceEditHistoryItem(String email, Date editTime) {
        this.email = email;
        this.editTime = editTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getEditTime() {
        return editTime;
    }

    public void setEditTime(Date editTime) {
        this.editTime = editTime;
    }
}
