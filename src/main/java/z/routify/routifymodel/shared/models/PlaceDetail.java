package z.routify.routifymodel.shared.models;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * User: Zun
 * Date: 6/11/14
 * Time: 6:55 PM
 */
@Entity("placedetail")
public class PlaceDetail{
    private Map<String, String> addressComponents;
    List<PlaceOpenHour> openingHours;
    private List<PlacePhoto> photos = new ArrayList<PlacePhoto>();
    private List<PlaceReview> reviews;
    private List<String> types;
    private String htmlAddress;
    private String address;
    private Location location;
    private String icon;
    @Id
    @org.jongo.marshall.jackson.oid.Id
    private org.bson.types.ObjectId id;
    private String phoneNumber;
    private String name;
    private double rating;
    private int priceLevel;
    private boolean permanentlyClosed;
    private String reference;
    private String url;
    private int userRatingsTotal;
    private int utcOffset;
    private String vicinity;
    private String website;
    private String description;
    private boolean disable;

    public Map<String, String> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(Map<String, String> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getHtmlAddress() {
        return htmlAddress;
    }

    public void setHtmlAddress(String htmlAddress) {
        this.htmlAddress = htmlAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public org.bson.types.ObjectId getId() {
        return id;
    }

    public void setId(org.bson.types.ObjectId id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PlaceOpenHour> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<PlaceOpenHour> openingHours) {
        this.openingHours = openingHours;
    }

    public List<PlacePhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PlacePhoto> photos) {
        this.photos = photos;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<PlaceReview> getReviews() {
        return reviews;
    }

    public void setReviews(List<PlaceReview> reviews) {
        this.reviews = reviews;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUserRatingsTotal() {
        return userRatingsTotal;
    }

    public void setUserRatingsTotal(int userRatingsTotal) {
        this.userRatingsTotal = userRatingsTotal;
    }

    public int getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(int utcOffset) {
        this.utcOffset = utcOffset;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public boolean isPermanentlyClosed() {
        return permanentlyClosed;
    }

    public void setPermanentlyClosed(boolean permanentlyClosed) {
        this.permanentlyClosed = permanentlyClosed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }
}