package z.routify.routifymodel.shared.models.tripPlan;

/**
 * User: zWork
 * Date: 15.8.2014
 * Time: 10:22
 */
public class Weather {
    private double temperature;
    private String text;
    private String icon;

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
