package z.routify.routifymodel.shared.models.tripPlan;

/**
 * Created by CHRIS on 9/08/2014.
 */
public enum TravelMode {
    DRIVING, WALKING, BICYCLING, PUBLIC_TRANSIT
}
