package z.routify.routifymodel.shared.models;

import java.util.ArrayList;

/**
 * User: Zun
 * Date: 6/11/14
 * Time: 6:38 PM
 */
public class PlacePhoto {
    private ArrayList html_attributions;
    private int height;
    private int width;
    private String photo_reference;
    private String thumbUrl;
    private String fullUrl;
    private boolean enable = true;


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public ArrayList getHtml_attributions() {
        return html_attributions;
    }

    public void setHtml_attributions(ArrayList html_attributions) {
        this.html_attributions = html_attributions;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
