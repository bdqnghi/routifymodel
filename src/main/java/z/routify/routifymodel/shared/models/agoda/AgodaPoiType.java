package z.routify.routifymodel.shared.models.agoda;

public class AgodaPoiType {
    private int poiType;
    private String poiTypeName;

    public int getPoiType() {
        return poiType;
    }

    public void setPoiType(int poiType) {
        this.poiType = poiType;
    }

    public String getPoiTypeName() {
        return poiTypeName;
    }

    public void setPoiTypeName(String poiTypeName) {
        this.poiTypeName = poiTypeName;
    }
}
