package z.routify.routifymodel.shared.models.tripPlan;

import org.mongodb.morphia.annotations.Id;
import z.routify.routifymodel.shared.models.Location;

import java.util.List;

/**
 * Created by CHRIS on 9/08/2014.
 */
public class TripLeg {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    @org.jongo.marshall.jackson.oid.ObjectId
    private String id;
    private int index;
    private String travelMode;
    private CompositeValue estimatedTime;
    private CompositeValue plannedTime;
    private CompositeValue distance;
    private double fee;
    private String geoWkt;
    private List<List<Location>> geoData;
    private List<Location> waypoints;
    private List<LegStep> steps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public CompositeValue getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(CompositeValue estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public CompositeValue getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(CompositeValue plannedTime) {
        this.plannedTime = plannedTime;
    }

    public CompositeValue getDistance() {
        return distance;
    }

    public void setDistance(CompositeValue distance) {
        this.distance = distance;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getGeoWkt() {
        return geoWkt;
    }

    public void setGeoWkt(String geoWkt) {
        this.geoWkt = geoWkt;
    }

    public List<List<Location>> getGeoData() {
        return geoData;
    }

    public void setGeoData(List<List<Location>> geoData) {
        this.geoData = geoData;
    }

    public List<Location> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Location> waypoints) {
        this.waypoints = waypoints;
    }

    public List<LegStep> getSteps() {
        return steps;
    }

    public void setSteps(List<LegStep> steps) {
        this.steps = steps;
    }
}
