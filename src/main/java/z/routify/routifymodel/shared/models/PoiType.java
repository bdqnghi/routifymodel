package z.routify.routifymodel.shared.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * User: Zun
 * Date: 6/8/14
 * Time: 5:34 PM
 */
@Entity("poitype")
public class PoiType {
    @Id private ObjectId poiTypeId;
    private int poiType;
    private String poiTypeName;

    public PoiType() {
    }

    public PoiType(int poiType, String poiTypeName) {
        this.poiType = poiType;
        this.poiTypeName = poiTypeName;
    }

    public ObjectId getPoiTypeId() {
        return poiTypeId;
    }

    public void setPoiTypeId(ObjectId poiTypeId) {
        this.poiTypeId = poiTypeId;
    }

    public int getPoiType() {
        return poiType;
    }

    public void setPoiType(int poiType) {
        this.poiType = poiType;
    }

    public String getPoiTypeName() {
        return poiTypeName;
    }

    public void setPoiTypeName(String poiTypeName) {
        this.poiTypeName = poiTypeName;
    }
}
