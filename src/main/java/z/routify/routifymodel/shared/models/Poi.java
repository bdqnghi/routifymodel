package z.routify.routifymodel.shared.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

/**
 * User: Zun
 * Date: 6/8/14
 * Time: 5:34 PM
 */
@Entity("poi")
public class Poi {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    @org.jongo.marshall.jackson.oid.ObjectId
    private ObjectId poiId;
    private String name;
    @Reference private PoiType type;
    @Embedded private Location location;
    private String city;
    private String provice;

    public ObjectId getPoiId() {
        return poiId;
    }

    public void setPoiId(ObjectId poiId) {
        this.poiId = poiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PoiType getType() {
        return type;
    }

    public void setType(PoiType type) {
        this.type = type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvice() {
        return provice;
    }

    public void setProvice(String provice) {
        this.provice = provice;
    }
}
