package z.routify.routifymodel.shared.models.user;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Zun
 * Date: 9/12/2014
 * Time: 11:30 PM
 */
@Entity("role")
public class Role {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    private org.bson.types.ObjectId id;
    private String name;
    private List<ObjectId> permissionIds = new ArrayList<ObjectId>();

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ObjectId> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<ObjectId> permissionIds) {
        this.permissionIds = permissionIds;
    }
}