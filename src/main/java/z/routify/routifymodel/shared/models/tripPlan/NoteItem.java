package z.routify.routifymodel.shared.models.tripPlan;

import z.routify.routifymodel.shared.models.Location;

/**
 * User: zWork
 * Date: 15.8.2014
 * Time: 10:15
 */
public class NoteItem {
    private String name;
    private String content;
    private double fee;
    private Location location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
