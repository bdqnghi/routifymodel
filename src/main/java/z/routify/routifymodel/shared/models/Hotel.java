package z.routify.routifymodel.shared.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * User: Zun
 * Date: 6/8/14
 * Time: 4:01 PM
 */
@Entity("hotel")
public class Hotel {
    @Id
    @org.jongo.marshall.jackson.oid.Id
    @org.jongo.marshall.jackson.oid.ObjectId
    private ObjectId hotelId;
    private Long originalId;
    private String name;
    private String imagelink;
    private double rating;
    private String briefDesc;
    private int commentCount;
    @Embedded private Location location;
    private String detailLink;
    private String cityName;
    private String proviceName;

    public Hotel() {
    }

    public Hotel(Long originalId, String name, String imagelink, double rating, String briefDesc, int commentCount, Location location, String detailLink, String cityName, String proviceName) {
        this.originalId = originalId;
        this.name = name;
        this.imagelink = imagelink;
        this.rating = rating;
        this.briefDesc = briefDesc;
        this.commentCount = commentCount;
        this.location = location;
        this.detailLink = detailLink;
        this.cityName = cityName;
        this.proviceName = proviceName;
    }

    public ObjectId getHotelId() {
        return hotelId;
    }

    public void setHotelId(ObjectId hotelId) {
        this.hotelId = hotelId;
    }

    public Long getOriginalId() {
        return originalId;
    }

    public void setOriginalId(Long originalId) {
        this.originalId = originalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getBriefDesc() {
        return briefDesc;
    }

    public void setBriefDesc(String briefDesc) {
        this.briefDesc = briefDesc;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getDetailLink() {
        return detailLink;
    }

    public void setDetailLink(String detailLink) {
        this.detailLink = detailLink;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProviceName() {
        return proviceName;
    }

    public void setProviceName(String proviceName) {
        this.proviceName = proviceName;
    }
}