package z.routify.routifymodel.shared.models;

/**
 * User: Zun
 * Date: 6/11/14
 * Time: 6:46 PM
 */
public class PlaceOpenHour {
    private PlaceTime close;
    private PlaceTime open;

    public PlaceTime getClose() {
        return close;
    }

    public void setClose(PlaceTime close) {
        this.close = close;
    }

    public PlaceTime getOpen() {
        return open;
    }

    public void setOpen(PlaceTime open) {
        this.open = open;
    }
}
