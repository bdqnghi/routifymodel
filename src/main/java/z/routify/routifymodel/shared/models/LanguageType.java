package z.routify.routifymodel.shared.models;

public class LanguageType {
    public static final String EN = "en_US";
    public static final String RU = "ru_RU";
    public static final String FI = "fi_FI";
    public static final String DE = "de_DE";

    public static boolean isValidLanguage(String language){
        return  EN.equals(language)
            ||  RU.equals(language)
            ||  FI.equals(language)
            ||  DE.equals(language);
    }
}
